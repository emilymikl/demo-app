import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.less']
})
export class ServerComponent implements OnInit {

	serverID = 10; //OR serverID: number = 10; Typescript will infer the types though, so not necessary
	serverStatus = "offline";

	getServerStatus() {
		return this.serverStatus;
	}

	getColor() {
		return this.serverStatus === 'online' ? 'green' : 'red';
	}

	constructor() {
		this.serverStatus = Math.random() > 0.5 ? 'online' : 'offline';
	 }

	ngOnInit() {
	}

}
