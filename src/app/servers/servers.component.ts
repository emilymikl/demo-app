import { Component, OnInit } from '@angular/core';

@Component({
 // selector: '[app-servers]', //Square brackets make this an attribute instead of a selector
  selector: '.app-servers', //The dot in front makes it a class to use like css classes
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.less']
})
export class ServersComponent implements OnInit {

	allowNewServer = false;
	serverCreationStatus = 'No server was created';
	serverName = '';
	serverCreated = false;
	servers = ['Testserver', 'Testserver 2'];

	//Constructor method executed at the time the component is executed by Angular
	constructor() {
		setTimeout( () => {
			this.allowNewServer = true;
		}, 2000);
	 }

	ngOnInit() {
	}

	onCreateServer() {
		this.serverCreated = true;
		this.servers.push(this.serverName);
		this.serverCreationStatus = 'Server was created! Name is ' + this.serverName;
	}

	onUpdateServerName(event: any) {
		this.serverName = event.target.value;
		//Can do explicit type casting of the event like this:
		// (<HTMLInputElement>event.target).value;
	}

}
